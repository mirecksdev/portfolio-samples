# Code samples

The code samples below are provided for general review purposes only. Please note that I have only included portions/partials that most succinctly represent work on select past projects.


## Rate calculator
Source code: [HTML](calculator/calc.html?at=master) | [CSS](calculator/calc.css?at=master) | [JS](calculator/calc.js?at=master) | [JSON](calculator/calc-json.js?at=master)

I was the sole developer on this project, which was created to allow customers to input key billing determinants in order to get an overall forecast of what their bill may be. Upon calculation, tabs are presented to show the bill breakdown, and the Base Rate tab provides visual "buckets" which "fill up" based on what values were provided by the user. Rates and percentages change dynamically if a user updates an individual field. Multiple customer types are supported, each having its own JSON object from which data is retrieved.

[https://www.georgiapower.com/business/billing-and-rates/power-and-light-calc.html]


## Video playlist
Source code: [HTML](playlist/playlist.html?at=master) | [CSS](playlist/playlist.css?at=master) | [JS](playlist/playlist.js?at=master)

I enhanced the site-wide HTML5 video player to include playlist support. This feature was added to the core player to enable filmstrip/auto-advance behavior, item grouping, tooltips, and custom labeling.

_This video functionality has since been phased-out across webmd.com, so a working example is not available._


## Health Heroes
Source code: [HTML](heroes/heroes.html?at=master) | [CSS](heroes/heroes.css?at=master) | [JS](heroes/heroes.js?at=master)

I authored all modules within the content area of the page, including the dynamic lead (rotating promo) module that appears as the first component in the center well.  The script includes support for a voting overlay and form submission, optimized for desktop and mobile.

[http://www.webmd.com/healthheroes/]


## Allergy app promo
Source code: [HTML](promo/promo.html?at=master) | [CSS](promo/promo.css?at=master)

I was responsible for all markup and styles of the promo page.  The email form hooks into an API service and was built by multiple developers, which is why JavaScript is not applicable for me on this page.

[http://www.webmd.com/allergy-app-promo]