
{
    "customer_size": "medium",
    "base_rates": {
        "basic_service": 19.00,
        "min_bill_kW": 8.24,
        "kVAR_rate": 0,
        "hud_buckets": {
            "bucket_A_levels": [{
                "name": "A1",
                "kWh": 3000,
                "rate": 11.2561
            }, {
                "name": "A2",
                "kWh": 7000,
                "rate": 10.3091
            }, {
                "name": "A3",
                "kWh": 190000,
                "rate": 8.8885
            }, {
                "name": "A4",
                "kWh": 200000,
                "rate": 6.8955
            }],
            "extra_bucket_rates": [{
                "name": "B",
                "rate": 1.1437
            }, {
                "name": "C",
                "rate": 0.8606
            }, {
                "name": "D",
                "rate": 0.7486
            }]
        }
    },
    "additional_rates": {
        "eccr": 12.7680,
        "nccr": 9.7357,
        "dsmr": 1.6721,
        "dsmc": 2.4471,
        "mff": {
            "inside": 2.9989,
            "outside": 1.1525
        },
        "fcr": [{
            "distribution": "secondary",
            "summer": 3.1718,
            "winter": 2.8813
        }, {
            "distribution": "primary",
            "summer": 3.1440,
            "winter": 2.8560
        }, {
            "distribution": "transmission",
            "summer": 3.1174,
            "winter": 2.8318
        }]
    }
}