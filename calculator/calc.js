/**
 * Power & Light Rate Calculator tool
 * @author Michelle Brewer
 * @version 1.0.0
 */

var gpcRateCalculator = {

    elements: {
        form: "#calculation-form",
        billingDemand: "#billing-demand",
        totalUsage: "#total-usage",
        cityLimits: "#city-limits",
        customerType: "#customer-type",
        distributionType: "#distribution-type",
        fcrSeason: "#fcr-season",
        salesTax: "#sales-tax"
    },

    init: function () {
        this.isStandard = null;
        this.submitted = false;
        this.fieldSelections = {};
        this.dataCollection = {};
        this.hudBuckets = {};
        this.amounts = {};

        this.$fields = {
            $demand: $(this.elements.billingDemand),
            $usage: $(this.elements.totalUsage)
        };

        this.bindEvents();

        return this;
    },

    bindEvents: function () {
        var self = this;

        $(self.elements.form).on("click", "#calculate-total", function(evt) {
            evt.preventDefault();
            self.submissions.validateValues(evt.target);

            if (!$(".field", $(self.elements.form)).hasClass("invalid")) {
                $("#calculation-tabs-wrap, #calculate-total, #bill-total").toggle();
                $("#calculation-content-wrap").hide();

                self.submissions.loadData(self.submissions.getCustomerType());
                self.submitted = true;
            }
        });

        $(self.elements.form).on("change", ".field", function (evt) {
            if (evt.target.type === "text") {
                self.submissions.validateValues(evt.target);
            }
            if (evt.target.type === "select-one" && self.submitted) {
                self.submissions.detectUpdates(evt.target);
            }
        });

        $('.tip-icon').each(function() {
            $(this).qtip({
                content: {
                    text: $(this).next('.tip-content')
                },
                position: {
                    my: "bottom right",
                    at: "top right",
                    adjust: { method: "none" }
                }
            });
        });
    },

    submissions: {
        /*
         * Check input fields to ensure valid numbers have been entered and indicate 
         * any errors by highlighting those fields. If a decimal is detected, round 
         * down before validating so that only a whole number is accepted.
         *
         * @param {Object} el - Source element
         */
        validateValues: function (el) {
            var self = gpcRateCalculator;

            var answer, $field, button;

            button = el.type === "button";
            $field = (button) ? self.$fields.$usage : $(el);

            answer = parseFloat($field.val());

            if (isNaN(answer) || !answer) {
                $field.toggleClass("invalid", true);
            } else {
                $field.val(Math.floor(answer));
                $field.toggleClass("invalid", false);
                
                if (self.submitted) {
                    self.submissions.detectUpdates(el);
                }
            }
        },

        /*
         * Execute method based on which input field receives the entry.
         * Data collection request is only triggered when billing demand 
         * or customer type changes.
         * 
         * @param {Object} field - Source element
         */
        detectUpdates: function (field) {
            var self = gpcRateCalculator;

            if (field.id !== "billing-demand" && field.id !== "customer-type") {
                var standardBill = self.calculateStandardBill();
                var minimumBill = self.calculateMinimumBill();

                if (minimumBill > standardBill) {
                    self.calculateMinimumBill();
                    $("#hud-buckets, #hud-bucket-rates").hide();
                    $("#minimum-kw-bill").html(self.dataCollection.base_rates.min_bill_kW);
                    $("#alt-min").show();
                } else {
                    self.calculateStandardBill();
                    $("#hud-buckets, #hud-bucket-rates").show();
                    $("#alt-min").hide();
                }

                self.compareBills();
            } else {
                self.submissions.loadData(this.getCustomerType());
            }

            gpcDebug("UPDATED FIELD DETECTED: " + field.id);
        },

        /*
         * Identify customer type associated with the current billing demand value
         * so the corresponding properties can be accessed (there is a separate
         * JSON object for each type.)
         * 
         * @returns {String} File name
         */
        getCustomerType: function () {
            var self = gpcRateCalculator;

            var customer = "", size= "", isSmall, isMedium;

            var demand = $(self.$fields.$demand).val();
            var type = $(self.elements.customerType).val();

            isSmall = demand < 30;
            isMedium = demand < 500;
            size = isSmall ? "_small" : (isMedium ? "_medium" : "_large");

            return customer.concat(type, size);
        },

        /*
         * Fetch single data file for the particular customer type that has been
         * determined. Once the data has been captured, do calculations.
         * 
         * @param {String} customer - File name
         */
        loadData: function (customer) {
            var self = gpcRateCalculator;

            $.ajax({
                type: "GET",
                dataType: "json",
                url: "js/customers/" + customer + ".js",
                success: function (data) {
                    self.dataCollection = data;

                    gpcDebug("DATA COLLECTION LOADED: " + customer);
                    gpcDebug(data);
                },
                complete: function () {
                    self.compareBills();
                }
            });
        }
    },

    /*
     * Methods used to determine the HUD (hours use demand) portion of the
     * base rate. HUD is made of up 1 primary and 3 additional "buckets."
     */
    hud: {
        /*
         * Isolate kWh of the first bucket, which has multiple rates based on
         * quantity utilized.
         *
         * @param {Number} selections - Accumulative kWh that comprises first bucket total
         *
         * @returns {Object[]} Array of kWh quantities per rate
         */
        firstBucket: function (selections) {
            var self = gpcRateCalculator;

            var bucketA = self.hud.setDistributionLevels(selections.totalUsage, selections.billingDemand * 200);
            var levelsA = self.dataCollection.base_rates.hud_buckets.bucket_A_levels;

            var fillLevel = 0;
            var ratesSum = 0;

            $.each(bucketA, function(index, value) {
                fillLevel += bucketA[index];
                ratesSum += levelsA[index].rate * bucketA[index];
            });

            self.hudBuckets.fillLevelA = fillLevel;

            return ratesSum;
        },

        /*
         * Assign the remaining kWh quantity (value that exceeded the first rate group)
         * to the 3 additional buckets available if needed.
         * 
         * @param {Number} maxBucketKWh - Total billing demand
         * @param {Number} carryOverKWh - Quantity that did not fit into first bucket
         * @param {Number} extraBucketsdRequired - How many additional buckets are needed
         *
         * @returns {Object[]} Array of kWh quantities per rate
         */
        extraBuckets: function (maxBucketKWh, carryOverKWh, extraBucketsRequired) {
            var self = gpcRateCalculator;

            var bucketsAvailable = extraBucketsRequired <= 3 ? extraBucketsRequired : 3;
            var runningTotal = carryOverKWh;
            var bucketCapacity = [];
            var ratesSum = [];
                
            var rates = gpcRateCalculator.dataCollection.base_rates.hud_buckets.extra_bucket_rates;

            if (extraBucketsRequired) {
                while (bucketsAvailable > 1) {
                    bucketCapacity.push(maxBucketKWh);
                    runningTotal -= maxBucketKWh;
                    bucketsAvailable--;
                }
                bucketCapacity.push(runningTotal);
            }

            self.hudBuckets.fillLevelB = bucketCapacity[0] || 0;
            self.hudBuckets.fillLevelC = bucketCapacity[1] || 0;
            self.hudBuckets.fillLevelD = bucketCapacity[2] || 0;

            ratesSum = [
                bucketCapacity[0] * rates[0].rate || 0,
                bucketCapacity[1] * rates[1].rate || 0,
                bucketCapacity[2] * rates[2].rate || 0
            ];

            return ratesSum;
        },

        /*
         * Save individual bucket costs as an array to the calculations property,
         * then add bucket values together to get full cost of hours usage demand.
         * 
         * @param {Object} selections - Form field values entered by user
         *
         * @returns {Number} HUD cost portion of base rate
         */
        tallyAllBuckets: function (selections) {
            var self = gpcRateCalculator;

            var maxBucketKWh = selections.billingDemand * 200;
            var carryOverKWh = selections.totalUsage - maxBucketKWh;
            var extraBucketsRequired = Math.floor(selections.totalUsage / maxBucketKWh);

            var totalFirstBucket = self.hud.firstBucket(selections);
            var totalExtraBuckets = self.hud.extraBuckets(maxBucketKWh, carryOverKWh, extraBucketsRequired);

            var totalSum = self.amounts.costPerBucket = [
                self.cost(totalFirstBucket),
                self.cost(totalExtraBuckets[0]),
                self.cost(totalExtraBuckets[1]),
                self.cost(totalExtraBuckets[2])
            ];

            totalSum = totalSum.reduce(function(a, b) { return a + b; });
            totalSum = parseFloat(totalSum.toFixed(2));

            return totalSum;
        },

        /*
         * Identify quantities within first bucket (calculated per its 4 separate
         * distribution levels) by reducing the running total upon each rate check.
         * Once the running total reaches zero, the distribution levels have been set.
         *
         * @param {Number} totalUsage - Value from Monthly Usage input field
         * @param {Number} maxBucketKWh - Total billing demand
         *
         * @returns {Object[]} Array of total kWh per distribution level
         */
        setDistributionLevels: function (totalUsage, maxBucketKWh) {
            var levelIndex = 0;
            var levelTotals = [];

            var bucketALevels = gpcRateCalculator.dataCollection.base_rates.hud_buckets.bucket_A_levels;
            var levelMaxAmount = bucketALevels[levelIndex].kWh;
            var runningTotal = (totalUsage < maxBucketKWh) ? totalUsage : maxBucketKWh;

            while (runningTotal >= 0) {
                if (levelMaxAmount > maxBucketKWh || runningTotal < levelMaxAmount) {
                    levelTotals.push(runningTotal);
                    return levelTotals;
                } else {
                    levelTotals.push(levelMaxAmount);
                    runningTotal -= levelMaxAmount;
                    levelIndex++; levelMaxAmount = bucketALevels[levelIndex].kWh;
                }
            }
        },

        /*
         * Use background image positioning to control the "fill" display for each
         * bucket in relation to the bucket's billing demand percentage.
         */
        getFillLevels: function () {
            var self = gpcRateCalculator;

            var el, percentOf, fillTo, position;
            
            var b = self.hudBuckets;
            var $w = $("#hud-buckets .bucket-wrap");
            var demand = parseInt($(self.elements.billingDemand).val() * 200);
            var level = [b.fillLevelA, b.fillLevelB, b.fillLevelC, b.fillLevelD];
            var block = ["blockA", "blockB", "blockC", "blockD"];

            for (var i = 0; i < 4; i++) {
                el = ".fill-line:nth-child(" + i + ")";
                percentOf = parseFloat(level[i] / demand).toFixed(2);
                fillTo = (percentOf > 1.00) ? 115 : parseInt(115 * percentOf);
                position = "-" + fillTo + "px";

                $w.find(".fill-line").each(function () {
                    if ($(this).hasClass(block[i])) {
                        $(this).css("background-position-y", position);
                    }
                });
            }
        },

        /*
         * Store names of selectors that correspond with each cost to be displayed and
         * call methods that set the final presentation elements of the buckets.
         */
        fillBuckets: function () {
            var self = gpcRateCalculator;

            var buckets = {
                "#bucket-A .cost .amt": self.amt(self.amounts.costPerBucket[0]),
                "#bucket-A .usage .amt": self.amt(self.hudBuckets.fillLevelA),
                "#bucket-B .cost .amt": self.amt(self.amounts.costPerBucket[1]),
                "#bucket-B .usage .amt": self.amt(self.hudBuckets.fillLevelB),
                "#bucket-C .cost .amt": self.amt(self.amounts.costPerBucket[2]),
                "#bucket-C .usage .amt": self.amt(self.hudBuckets.fillLevelC),
                "#bucket-D .cost .amt": self.amt(self.amounts.costPerBucket[3]),
                "#bucket-D .usage .amt": self.amt(self.hudBuckets.fillLevelD)
            };

            var hideEmpty = true;

            self.hud.getFillLevels();
            self.render(buckets, hideEmpty);
        }
    },

    /*
     * Calculate values for common schedule fees and add together for full cost.
     * 
     * @param {Object} selections - Form field values entered by user
     *
     * @returns {Number} Other schedule fees total cost
     */
    calculateSchedules: function (selections) {
        var totalSum;

        var data = this.dataCollection.additional_rates;
        var calc = this.amounts;

        calc.eccr = this.cost(calc.baseRateSubTotal * data.eccr);
        calc.nccr = this.cost(calc.baseRateSubTotal * data.nccr);
        calc.dsm = this.cost(calc.baseRateSubTotal * data.dsm);
        calc.fcr = this.cost(selections.totalUsage * data.fcr[selections.distributionType][selections.fcrSeason]);

        totalSum = parseFloat((calc.eccr + calc.nccr + calc.dsm + calc.fcr).toFixed(2));

        return totalSum;
    },

    /*
     * Calculate single value for municipal franchise fee.
     * 
     * @param {String} cityLimits - Location choice specified by user
     *
     * @returns {Number} Fee total cost
     */
    calculateMFF: function (cityLimits) {
        var data = this.dataCollection.additional_rates, calc = this.amounts;
        var totalSum = this.cost(calc.subTotal * data.mff[cityLimits]);

        calc.mff = totalSum;

        return totalSum;
    },

    /*
     * Calculate sales tax.
     * 
     * @param {String} salesTax - Tax choice specified by user
     *
     * @returns {Number} Tax total cost
     */
    calculateTax: function (salesTax) {
        var taxRate = parseFloat(salesTax);
        var totalSum = parseFloat((this.amounts.preTaxSubTotal * taxRate).toFixed(2));

        this.amounts.salesTax = totalSum;

        return totalSum;
    },

    /*
     * Assign all calculated values as new properties of namespace for later
     * reference. Two bill types -- standard and minimum -- are calculated in tandem.
     * Whichever bill type ending up with the greatest total gets its info rendered
     * upon execution of the populateAllTabs() method.
     */
    calculateStandardBill: function () {
        var field = this.elements;
        var calc = this.amounts;

        var selections = {
            billingDemand: parseInt($(field.billingDemand).attr("value")),
            totalUsage: parseInt($(field.totalUsage).attr("value")),
            cityLimits: $(field.cityLimits).val(),
            distributionType: $(field.distributionType).val(),
            fcrSeason: $(field.fcrSeason).val(),
            salesTax: parseFloat($(field.salesTax).val())
        };

        calc.basicServiceCharge = this.dataCollection.base_rates.basic_service;
        calc.baseRateSubTotal = calc.basicServiceCharge + this.hud.tallyAllBuckets(selections);
        calc.subTotal = calc.baseRateSubTotal + this.calculateSchedules(selections);
        calc.preTaxSubTotal = calc.subTotal + this.calculateMFF(selections.cityLimits);
        calc.standardBillTotal = calc.preTaxSubTotal + this.calculateTax(selections.salesTax);
        calc.isStandard = true;

        return calc.standardBillTotal;
    },

    /*
     * Assign all calculated values as new properties of namespace for later
     * reference. Two bill types -- standard and minimum -- are calculated in tandem.
     * Whichever bill type ending up with the greatest total gets its info rendered
     * upon execution of the populateAllTabs() method.
     */
    calculateMinimumBill: function () {
        var field = this.elements;
        var calc = this.amounts;

        var selections = {
            billingDemand: parseInt($(field.billingDemand).attr("value")),
            totalUsage: parseInt($(field.totalUsage).attr("value")),
            cityLimits: $(field.cityLimits).val(),
            distributionType: $(field.distributionType).val(),
            fcrSeason: $(field.fcrSeason).val(),
            salesTax: parseFloat($(field.salesTax).val())
        };

        var isInExcess = selections.billingDemand >= 30;
        var isPLL = selections.billingDemand >= 500;
        var demand = selections.billingDemand;

        demand = isInExcess && !isPLL ? demand - 30 : !isInExcess ? 0 : demand;

        gpcDebug("BILLING DEMAND IN EXCESS OF 30? " + isInExcess);
        gpcDebug("IS TARIFF PLL? " + isPLL);
        gpcDebug("CALCULATE BILLING DEMAND as " + demand);

        calc.basicServiceCharge = this.dataCollection.base_rates.basic_service;
        calc.baseRateSubTotal = calc.basicServiceCharge + (this.dataCollection.base_rates.min_bill_kW * demand);
        calc.subTotal = calc.baseRateSubTotal + this.calculateSchedules(selections);
        calc.preTaxSubTotal = calc.subTotal + this.calculateMFF(selections.cityLimits);
        calc.minimumBillTotal = calc.preTaxSubTotal + this.calculateTax(selections.salesTax);
        calc.isStandard = false;

        return calc.minimumBillTotal;
    },

    /*
     * Compare standard vs minimum bill and call to populate tabs with the end results
     */
     compareBills: function () {
        var standardBill = this.calculateStandardBill();
        var minimumBill = this.calculateMinimumBill();

        if (minimumBill > standardBill) {
            this.calculateMinimumBill();
            $("#hud-buckets, #hud-bucket-rates").hide();
            $("#minimum-kw-bill").html(this.dataCollection.base_rates.min_bill_kW);
            $("#bill-total .verbiage").html("Your Minimum Bill Total");
            $("#alt-min").show();
        } else {
            this.calculateStandardBill();
            $("#hud-buckets, #hud-bucket-rates").show();
            $("#bill-total .verbiage").html("Your Bill Total");
            $("#alt-min").hide();
        }

        gpcDebug("STANDARD BILL $" + parseFloat(standardBill).toFixed(2));
        gpcDebug("MINIMUM BILL $" + parseFloat(minimumBill).toFixed(2));

        this.populateAllTabs();
     },

    /*
     * Generate all components to be displayed for Base Rate tab.
     */
    populateBaseRate: function () {
        var data = this.dataCollection.base_rates;
        var bucketA = data.hud_buckets.bucket_A_levels;
        var bucketsBCD = data.hud_buckets.extra_bucket_rates;

        var bucketALevels = {
            "#level-A1 .usage": bucketA[0].kWh.toLocaleString("en"),
            "#level-A1 .amt": bucketA[0].rate / 100,
            "#level-A2 .usage": bucketA[1].kWh.toLocaleString("en"),
            "#level-A2 .amt": bucketA[1].rate / 100,
            "#level-A3 .usage": bucketA[2].kWh.toLocaleString("en"),
            "#level-A3 .amt": bucketA[2].rate / 100,
            "#level-A4 .usage": bucketA[3].kWh.toLocaleString("en"),
            "#level-A4 .amt": bucketA[3].rate / 100
        };

        var extraBucketRates = {
            "#bucket-B-rate .amt": bucketsBCD[0].rate,
            "#bucket-C-rate .amt": bucketsBCD[1].rate,
            "#bucket-D-rate .amt": bucketsBCD[2].rate
        };

        var obj = $.extend({}, bucketALevels, extraBucketRates);

        $("#base-rate-total").html(this.amt(this.amounts.baseRateSubTotal));
        $("#basic-service-charge").html(data.basic_service.toFixed(2));

        this.hud.fillBuckets();
        this.render(obj);
    },

    /*
     * Generate all components to be displayed for Other Schedules tab.
     */
    populateSchedules: function () {
        var data = this.dataCollection.additional_rates;
        var calc = this.amounts;
        var sub = calc.baseRateSubTotal;

        var distribution = $(this.elements.distributionType + " option:selected").val();
        var season = $(this.elements.fcrSeason + " option:selected").val();
        var fcr = data.fcr[distribution][season];

        var totalUsage = parseInt($(this.elements.totalUsage).attr("value"));

        var schedules = {
            "#fcr-rate .kWh": $(this.elements.totalUsage).attr("value"),
            "#fcr-rate .rate": fcr,
            "#fcr-rate .amt": this.amt(totalUsage * (fcr / 100)),
            "#eccr-rate .percent": data.eccr,
            "#eccr-rate .amt": this.amt(sub * (data.eccr / 100)),
            "#nccr-rate .percent": data.nccr,
            "#nccr-rate .amt": this.amt(sub * (data.nccr / 100)),
            "#dsm-rate .percent": data.dsm,
            "#dsm-rate .amt": this.amt(sub * (data.dsm / 100))
        };

        $("#other-schedules-total").html(this.amt(calc.eccr + calc.nccr + calc.dsm + calc.fcr));

        this.render(schedules);
    },

    /*
     * Generate all components to be displayed for Municipal Franchise Fee tab.
     */
    populateMFF: function () {
        var data = this.dataCollection.additional_rates.mff;

        $("#franchise-fee-total").html(this.amt(this.amounts.mff));
        $("#inside-rate .percent").html(data.inside);
        $("#outside-rate .percent").html(data.outside);
    },

    /*
     * Generate total to be displayed for Sales Tax tab.
     */
    populateTax: function () {
        $("#sales-tax-total").html(this.amt(this.amounts.salesTax));
    },

    /*
     * Display bill total amount in place of calculate button.
     */
    populateBillTotal: function () {
        var calc = this.amounts;

        var final = (!calc.isStandard) ? calc.minimumBillTotal : calc.standardBillTotal;

        $("#bill-total .total").html(this.amt(final));
    },

    /*
     * Make each tab's total cost and associated content visible.
     */
    populateAllTabs: function () {
        this.populateBillTotal();
        this.populateBaseRate();
        this.populateSchedules();
        this.populateMFF();
        this.populateTax();
    },

    /*
     * Write HTML to specified elements and show content if not already visible.
     *
     * @param {Object} element - Key/value pair for each element name with its content
     * @param {Boolean} hideEmpty - If TRUE, use to clear display of pre-existing values
     */
    render: function (element, hideEmpty) {
        $.each(element, function (index, value) {
            var $parent = $(index).parent();
            var isEmpty = hideEmpty && !value || hideEmpty && value === "0";

            if (isEmpty) {
                $parent.toggleClass("hidden", true);
            } else {
                if ($parent.hasClass("hidden")) {
                    $parent.toggleClass("hidden", false);
                }
                $(index).html(value);
            }
        });
    },

    /*
     * Format specified portion of total bill for calculation.
     *
     * @param {Number} cost - Value to be parsed
     *
     * @returns {Number} New value in 0.00 format
     */
    cost: function (cost) {
        return parseFloat((cost / 100).toFixed(2));
    },

    /*
     * Convert value to readable dollar amount and add trailing zero
     * if resulting value does not contain 2 decimals. Include commas
     * to separate groups where appropriate.
     *
     * @param {Number} amt - Value to be parsed
     *
     * @returns {String} New value in 0.00 format
     */
    amt: function (amt) {
        var str, amount;

        var num = parseFloat(amt.toFixed(2));

        str = num.toLocaleString("en");

        if (str.indexOf(".") !== -1) {
            amount = str.split(".");

            if (amount[1].length !== 2) {
                str += "0";
            }
        }

        return str;
    }
};

var gpcDebugMode = true;
function gpcDebug(test) {
    if (!gpcDebugMode || !console) return;
    if (test && (typeof test === "string")) { console.log("debug >>>>   " + test); };
    if (test && (typeof test === "object")) { console.log(test); };
    if (test && (test instanceof Function)) { callback(); };
}