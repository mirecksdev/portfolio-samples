w.heroes = {
	init: function(){
		this.messages = {
			success: "Thanks for your vote!",
			fail: "We're sorry, but only one vote per day is allowed. Try again tomorrow.",
			results: "Return on Nov. 3 to see who was selected.",
			template: '<div class="{msgClass}"><p><strong>{msgHeader}</strong> {followUp}</p></div></div>'
		};
		this.$el = $('#vote-finalists');
		this.cookieId = 'hero';
		this.cookiePath = '/healthheroes/';
		this.events();
	},
	events: function(){
		var _this = this;
		this.$el.find('.cta-button').on('click', function(e){
			e.preventDefault();
			_this.vote($(this).attr('data-choice'));
		});
	},
	cookie: function(){
		var voted = cookie.get('hhero'), msg;
		if (!voted) {
			cookie.set('hero',true,{path:this.cookiePath,expires:1 });
			msg = 'success';
		} else {
			msg = 'fail';
		}
		this.message(msg);
	},
	vote: function(choice){
		var _this = this;
		$.ajax({
			type: 'POST',
			url: '/api/spec',
			data: {id: '4', pollChoiceSequenceId: choice},
			success: function(){
				_this.cookie();
			},
			error: function(){
				debug("error posting vote");
			}
		});
	},
	message: function(status){
		var body = sub(this.messages.template, {
			msgClass: status,
			msgHeader: this.messages[status],
			followUp: this.messages.results,
			privacy: this.messages.privacy,
			notify: this.messages.notification
		});
		var $overlay = $('#vote-overlay-content');
		$overlay.find('.message').append(body);
		w.overlay.open({
			width: '475px',
			scrolling: false,
			html: $overlay.parent().html(),
			onComplete: function(){
				require(['newsletter'], function(module) {
					module.init({
						selector: $('#nl-health-heroes-overlay'),
						promo: false,
						template: {
							content: '' +
								'<div class="nls-content">\n' +
								'<div class="notify"><h4>Get notified when the winners are announced:</h4></div>\n' +
								'<form class="nls-form" action="#" novalidate="novalidate">\n' +
								'<input type="email" class="nls-email" name="email" autocapitalize="off" autocorrect="off" placeholder="Enter Your Email Address" /> ' +
								'<button name="nl-submit" formnovalidate class="cta-button off {submitClass}" type="submit">Submit</button>\n' +
								'</form>\n' +
								'</div>',
							successMsg: '<p class="submit-complete success-msg">Thank you!</p>',
							failure: '<p class="submit-complete fail-msg">This feature is temporarily unavailable. Please try again later.</p>'
						}
					});
				});
				$('##vote-overlay-content').show();
				w.overlay.resize();
			},
			onClosed: function(){
				$overlay.find('.message').empty();
			}
		});
	}
};
$(function(){
	w.heroes.init();
});