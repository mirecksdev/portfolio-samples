w.edvid = {

    filmstripCount: 0,
    filmstripTrigger: null,

    init: function(chron, moduleid) {
        var self = this;

        this.playFirst = w.url.getParam('vid') || false;
        this.chron = chron;
        this.moduleid = moduleid;
        this.vidformat = 'm4v';
        this.vidname = 'w.edvid.video';

        if (!this.playFirst) {
            this.video = w.object(w.vidapi).init({
                chronicleID: this.chron,
                formatType: this.vidformat,
                metricName: this.moduleid,
                name: this.vidname
            });
            $(function() {
                self.cacheItems();
                self.attachEvents();
                self.loadPlaylist(self.chron);
            });
        } else {
            $(function() {
                self.cacheItems();
                self.attachEvents();
                self.loadPlaylist(self.chron);

                self.initChron(self.chron);
            });
        }

        return this;
    },

    initChron: function(chron) {
        var self = this,
            deepLinkHref = 'a[href="?vid=' + self.playFirst + '"]',
            deepLinkProps = {
                chronic: self.features.playlist.$allItems.find(deepLinkHref).attr('chronic'),
                group: $('.group').index($(deepLinkHref).parents('.group'))
            };

        if (deepLinkProps.chronic !== undefined) {
            self.filmstripCount = (deepLinkProps.group > 0) ? deepLinkProps.group : 0;
            self.filmstripTrigger = 'deeplink';
            self.newVideo(deepLinkProps.chronic);
            self.labelNowPlaying(deepLinkProps.chronic);
        } else {
            return null;
        }
    },

    cacheItems: function() {
        var self = this;

        self.containers = {
            $playerMain: $('.video_wrap'),
            $playlistMain: $('#playlist'),
            $playlistActive: $('#playlist-active'),
            $playlistHover: $('#playlist-hover'),
            $playlistPosition: $('#playlist #position')
        };
        self.features = {
            player: {
                $videoTitle: $('.video_summary h4', self.containers.$playerMain),
                $videoDescription: $('.video_summary p', self.containers.$playerMain),
                $captionControl: $('.video_cc_button', self.containers.$playerMain),
                $captionOverlay: $('.video_caption', self.containers.$playerMain)
            },
            playlist: {
                $dynamicLinks: $('a', self.containers.$playlistMain.filter('.dynamic')),
                $dynamicList: self.containers.$playlistMain.hasClass('dynamic'),
                $allItems: $('.item', self.containers.$playlistActive),
                $itemThumbs: $('.playlist-thumb', self.containers.$playlistActive),
                $itemSummary: $('#position', self.containers.$playlistMain),
                $navigation: $('.next-control:eq(0), .prev-control:eq(0)', self.containers.$playlistMain),
                activeItem: 'playlist-thumb_on',
                itemLabel: '<div class="playing"><strong>Now Playing</strong></div>',
                countThumbGroups: $('.group', self.containers.$playlistMain).length,
                countAllThumbs: $('.item', self.containers.$playlistMain).length
            }
        };
    },

    attachEvents: function() {
        var self = this,
            counter,
            isVisible,
            thisArrow;

        $(document).bind('newVideo', function(e) {
            self.video = w.object(w.vidapi).init({
                chronicleID: e.chron,
                formatType: self.vidformat,
                metricName: self.moduleid,
                name: self.vidname
            });
        });

        $(document).bind('endVideo', function() {
            if (self.containers.$playlistMain.length < 1) {
                self.changeVideo(self.chron);
            } else {
                self.playNextVideo();
            }
        });

        self.features.playlist.$dynamicLinks.click(function(e) {
            e.preventDefault();

            if (!$(this).hasClass(self.features.playlist.activeItem)) {
                self.changeVideo($(this).attr('chronic'));
            }
        });

        self.features.playlist.$navigation.click(function() {
            thisArrow = $(this).attr('class').substring(0, 4);
            isVisible = $(this).hasClass(thisArrow + '-control-on');
            counter = (isVisible) ? ((thisArrow === 'next') ? ++self.filmstripCount : --self.filmstripCount) : self.filmstripCount;

            self.filmstripController(counter, {
                direction: thisArrow,
                enabled: isVisible
            });
        });
    },

    filmstripController: function(counter, arrow) {
        var self = this,
            fromCounter = 1,
            toCounter = 2,
            groupBy = 3,
            finalGroup = self.features.playlist.countAllThumbs - 2,
            on = '-control-on',
            thisArrow = arrow.direction,
            oppositeArrow = (thisArrow === 'next') ? 'prev' : 'next',
            maxCount = self.features.playlist.countThumbGroups - 1,
            $arrows = self.features.playlist.$navigation,

            _checkState = function(direction) {
                return {
                    visibility: $arrows.filter('.' + direction + '-control').hasClass(direction + on),
                    rangeLimit: (direction === 'next') ? counter === maxCount : counter === 0
                };
            },

            _getGroupProperties = function(group) {
                var countInGroup = $('.group:eq(' + group + ')', self.containers.$playlistMain).find('.item').length,
                    isFinal = countInGroup % groupBy !== 0;

                return {
                    index: group,
                    items: countInGroup,
                    from: (group) ? fromCounter += ((isFinal) ? finalGroup : countInGroup * group) : 1,
                    to: (group) ? toCounter += ((isFinal) ? finalGroup : fromCounter) : 3
                };
            },

            _scrollToGroup = function(direction, group) {
                var options = {
                        calc: (direction === 'next') ? '-=' : '+=',
                        offset: 441,
                        scroll: group,
                        trigger: self.filmstripTrigger
                    },
                    _setMargin = function() {
                        var margin = options.offset;

                        if (options.trigger === 'deeplink' || options.trigger === 'furl') {
                            margin *= options.scroll;
                        } else if (options.trigger === 'autoplay' && options.scroll === 0) {
                            margin *= maxCount;
                        }

                        return margin;
                    };

                self.containers.$playlistActive.animate({
                    marginLeft: options.calc + _setMargin() + 'px'
                }, 'medium');

                self.filmstripTrigger = null;
            },

            _toggleArrows = function(direction, action) {
                var $filtered = $arrows.filter('.' + direction + '-control');

                return (action === 'addClass') ? $filtered.addClass(direction + on) : $filtered.removeClass(direction + on);
            },

            _updateSummary = function(group) {
                var $summary = self.containers.$playlistPosition;

                if (group.items > 1) {
                    $('#item-range', $summary).html(group.from + ' - ' + group.to);
                } else {
                    $('#item-range', $summary).html(self.features.playlist.countAllThumbs);
                }
            };

        if (arrow.enabled || (!arrow.enabled && self.filmstripTrigger === 'autoplay')) {
            if (!_checkState(oppositeArrow).visibility) {
                _toggleArrows(oppositeArrow, 'addClass');
            }
            if (_checkState(oppositeArrow).visibility && _checkState(thisArrow).rangeLimit) {
                _toggleArrows(thisArrow, 'removeClass');
            }

            _scrollToGroup(arrow.direction, counter);
            _updateSummary(_getGroupProperties(counter));
        }
    },

    formatChar: function(el) {
        return el.replace(/\®|&reg;/g, '<sup>&reg;</sup>');
    },

    formatPlaylistTips: function() {
        var self = this,
            title, description,
            $tip = self.containers.$playlistHover;

        self.features.playlist.$allItems.hoverIntent({
            over: function() {
                title = $('p', this).html();
                description = self.formatChar($('a', this).attr('desc'));

                $('.content a', $tip).attr('href', $('a', this).attr('href')).html(title);
                $('.content p', $tip).html(description);

                if ($(this).index() === 2) {
                    $tip.addClass('last').css('left', '100px').fadeIn('fast');
                } else {
                    var position = ($(this).index() === 0) ? 170 : 315;
                    $tip.css('left', position + 'px').fadeIn('fast');
                }
            }, timeout: 100,
            out: function() {
                $tip.fadeOut('fast').attr('style', '').removeClass('last');
            }
        });
    },

    labelNowPlaying: function(chron) {
        var self = this,
            currentUrl = w.url.clear(),
            hasGroups = (self.features.playlist.countAllThumbs > 3) ? self.features.playlist.countThumbGroups : false,
            itemHref,
            itemIndex,
            isPlaying,
            thisGroup,

            _filterItems = function(item) {
                var indexOfGroup = $('.group').index($(item).parents('.group')),
                    isLeadGroup = indexOfGroup === 0 || false,
                    isLeadItem = $(item).index() === 0 || false,
                    itemProps = {
                        group: isLeadGroup ? 0 : indexOfGroup,
                        direction: isLeadGroup ? 'prev' : 'next'
                    };

                return isLeadItem ? itemProps : null;
            },

            _scrollToItem = function(item) {
                if (item.group !== self.filmstripCount) {
                    self.filmstripTrigger = 'autoplay';
                    self.filmstripCount = (item.group !== 0) ? ++self.filmstripCount : 0;

                    self.filmstripController(self.filmstripCount, {
                        direction: item.direction,
                        enabled: (item.direction === 'next') ? true : false
                    });
                }
            };

        self.features.playlist.$allItems.each(function() {
            isPlaying = false;
            itemIndex = $(this).index();

            $('a', this).each(function() {
                itemHref = this.href;

                if ((w.url.clear(itemHref) === currentUrl && itemHref.indexOf('?vid') === -1) || chron === $(this).attr('chronic')) {
                    isPlaying = true;
                }
            });

            if (!isPlaying) {
                $(this).removeClass('on').children('.playing').remove();
                $('a', this).removeClass(self.features.playlist.activeItem);
            } else {
                $(this).addClass('on').prepend(self.features.playlist.itemLabel).children('a').addClass(self.features.playlist.activeItem);

                thisGroup = $('.group').index($(this).parents('.group'));

                if (hasGroups) {
                    if (self.filmstripTrigger === 'deeplink' && self.filmstripCount !== 0) {
                        self.filmstripController(self.filmstripCount, {
                            direction: 'next',
                            enabled: (chron.group !== self.features.playlist.countThumbGroups - 1) ? true : false
                        });
                    } else if (itemHref.indexOf('?vid') === -1 && self.filmstripCount === 0 && thisGroup !== 0) {
                        self.filmstripCount = thisGroup;
                        self.filmstripTrigger = 'furl';
                        self.filmstripController(self.filmstripCount, {
                            direction: 'next',
                            enabled: true
                        });
                    } else {
                        if (_filterItems(this) !== null) {
                            _scrollToItem(_filterItems(this));

                        }
                    }
                }
            }
        });
    },

    loadPlaylist: function(chron) {
        var self = this;

        if (self.containers.$playlistMain.length > 0) {
            if ($('#playlist-shell').hasClass('ed-sp-mixed')) {
                self.features.player.$videoTitle.children('.spons-label').each(function() {
                    $(this).removeClass('off').addClass('on');
                });
            }
            self.features.playlist.$itemThumbs.find('p').each(function() {
                $(this).html(self.formatChar($(this).html()));
            });

            self.features.playlist.$itemThumbs.click(function() {
                if (!$(this).hasClass(self.features.playlist.activeItem)) {
                    self.sendMetricData($(this).attr('vidname'));
                } else {
                    return false;
                }
            });

            self.labelNowPlaying(chron);
            self.formatPlaylistTips();
        }
    },

    playNextVideo: function() {
        var self = this,
            itemCount = self.features.playlist.$itemThumbs.length,
            next,
            position,
            start = 1;

        self.features.playlist.$itemThumbs.each(function(i) {
            if ($(this).hasClass(self.features.playlist.activeItem)) {
                next = i + start;
                position = (next === itemCount) ? ':eq(0)' : ':eq(' + next + ')';
            }
        });

        if (self.features.playlist.$dynamicList) {
            self.changeVideo(self.features.playlist.$itemThumbs.filter(position).attr('chronic'));
        } else {
            window.open(self.features.playlist.$itemThumbs.filter(position).attr('href'), '_self');
        }
    }
};